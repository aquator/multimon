# Introduction

Multimon provides a way to set up multiple monitor setups for your notebook for single mode, office docking, and home screen.

The implementation reflects my usecases: an extra screen at home connected to HDMI, two extra screens via DisplayLink at office (not really tested since COVID19) and the single notebook display mode for on the go.

I am using [Arch Linux] with [i3wm], [Polybar] and [Conky], so that has a great impact on things.

# Usage

The script `multimon` auto-detects current monitors, and executes all the other scripts to set up everything accoording to current setup. `multimon --watch` will keep running and watch for changes, so it will auto-detect plugged in or unplugged devices, and respond. You can use explicit `multimon --enable` and `multimon --disable` to enable or disable multi monitor setup.

All the scripts must be in the PATH by default, but of course you can tweak the scripts with full path if you wish.

You need to tweak some of the scrips, or remove them - as you wish - to set things up, as defaults are tailored to my needs (for example my conky widgets in `multimon-conky`).

Scripts can be run individually, mostly accepting a single argument as the number of monitors to use, omitting the argument will auto-detect.

For example, in my `.xinitrc` I use the following:
````
"$HOME/bin/multimon-xrandr"
"$HOME/bin/multimon-wallpapers"
````
To set up the display properly before starting up `i3`, than in `i3` config I am starting up `multimon --watch` to keep auto-detect running, and set up everything else.

# Xresources

Configuration should be placed to .Xresources, the following values are expected:
````
multimon.monitor.notebook: eDP-1
multimon.monitor.extLeft:  DVI-I-1-1
multimon.monitor.extRight: DVI-I-2-2
multimon.monitor.hdmi:     DP-1
multimon.wallpapers.1: <path-to-wallpaper>
multimon.wallpapers.2: <path-to-wallpaper-1> <path-to-wallpaper-2>
multimon.wallpapers.3: <path-to-wallpaper-1> <path-to-wallpaper-2> <path-to-wallpaper-3>

! Default Workspace Names
i3wm.ws0.name: 0:Workspace0
i3wm.ws1.name: 1:Workspace1
i3wm.ws2.name: 2:Workspace2
i3wm.ws3.name: 3:Workspace3
i3wm.ws4.name: 4:Workspace4
i3wm.ws5.name: 5:Workspace5
i3wm.ws6.name: 6:Workspace6
i3wm.ws7.name: 7:Workspace7
i3wm.ws8.name: 8:Workspace8
i3wm.ws9.name: 9:Workspace9
````

To set up different inital screens for i3wm workspaces a custom resource file is merged based on the number of screens.

### .Xresources_1_screens
````
! Workspace Output for Single Screen Setup

i3wm*output: primary
````

### .Xresources_2_screens
````
! Workspace Output for Dual Screen Setup

i3wm*output: primary
i3wm.ws0.output: eDP-1
i3wm.ws3.output: eDP-1
````

### .Xresources_3_screens
````
! Workspace Output for Triple Screen Setup

i3wm*output: primary
i3wm.ws0.output: DVI-I-1-1
i3wm.ws9.output: DVI-I-2-2
````

Monitors are the xrandr ids for built in notebook screen, external screens connected via DisplayLink in 3 monitor mode, and the notebook's hdmi port for 2 monitor mode. You should set your values, and tweak `multimon-xrandr` for layout.

Wallpapers are set with `feh`.

# Unlicense

This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or
distribute this software, either in source code form or as a compiled
binary, for any purpose, commercial or non-commercial, and by any
means.

In jurisdictions that recognize copyright laws, the author or authors
of this software dedicate any and all copyright interest in the
software to the public domain. We make this dedication for the benefit
of the public at large and to the detriment of our heirs and
successors. We intend this dedication to be an overt act of
relinquishment in perpetuity of all present and future rights to this
software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>


[Arch Linux]: https://www.archlinux.org/
[i3wm]: https://i3wm.org/
[Polybar]: https://polybar.github.io/
[Conky]: http://conky.sourceforge.net/docs.html
